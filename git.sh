#!/bin/bash
#Check for argument
if [[ $# -eq 0 ]]; then
        echo "No arguments are passes"
fi
# if ran with pull fork
if [[ $1 == "pull fork" ]]; then
        git pull fork master

fi

# if pull origin
if [[ $1 == "pull origin" ]]; then
        git pull origin master
fi

# if push
if [[ $1 == "push" ]]; then
        git add .
        git commit -m "$2"
        git push fork $3
fi



